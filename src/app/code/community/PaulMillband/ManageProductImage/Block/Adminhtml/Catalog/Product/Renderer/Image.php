<?php 
class PaulMillband_ManageProductImage_Block_Adminhtml_Catalog_Product_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        return $this->_getValue($row);
    } 
    protected function _getValue(Varien_Object $row)
    {   
        $out = '';
        $val = $row->getData($this->getColumn()->getIndex());
        if( isset($val) && $val !== 'no_selection' ){
            $url = Mage::getBaseUrl('media') . 'catalog/product' . $val; 
            $out .= "<img src='". $url ."' width='".$this->getColumn()->getWidth(). "'/>"; 
        }
        return $out;
    }
}